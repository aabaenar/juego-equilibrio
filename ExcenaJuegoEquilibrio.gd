extends Node

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var i = 1
export (PackedScene) var Ball

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

func _process(delta):
	$Label.set_text("Acelerometro x:"+str(Input.get_accelerometer().x)+"\n"+
	"Acelerometro y:"+str(Input.get_accelerometer().y)+"\n"+
	"Acelerometro z:"+str(Input.get_accelerometer().z)+"\n")
	$Mundo.position.y = 2000
	$Mundo.position.x =540+ (sin(PI*OS.get_ticks_msec()/2500))*250
	
func _input(event):
	if event.is_action_pressed("click"):
		var new_ball = Ball.instance()
		new_ball.position = get_viewport().get_mouse_position()
		add_child(new_ball)